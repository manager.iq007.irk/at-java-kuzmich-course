import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserExpression {
    static List<String> userExpression = new ArrayList<>();
    public static final double MAX_VALUE = 1.7976931348623157E308;

    public static void checkExpression(String s) throws SpacesException, NumberException, MaxValueException, ValidException {
        while (userExpression.size() < 3) {
            for (String userExp : s.trim().split("\\s+")) {
                userExpression.add(userExp);
            }
        }
        // Для отладки: System.out.println("Полученные значения: " + userExpression);

        if (Objects.equals(userExpression.get(0), userExpression.get(1)) ||
                Objects.equals(userExpression.get(1), userExpression.get(2)))
            throw new SpacesException("Ошибка 003: не введены пробелы между значениями и арифметическим знаком, валидация не пройдена!");
        if (userExpression.size() != 3)
            throw new ValidException("Ошибка 004: не введено выражение с двумя значениями и одним действием!");
        try {
            if (Double.parseDouble(userExpression.get(0)) > MAX_VALUE || Double.parseDouble(userExpression.get(2)) > MAX_VALUE)
                throw new MaxValueException("Ошибка 005: Введенное значение преышает максимально допустимое");
        } catch (NumberFormatException e) {
            throw new NumberException("Ошибка 002: не введены числа для расчета");
        } finally {
            userExpression.clear();
        }
    }

    public static void doOperation(String s) throws SpacesException, NumberException, NullException {
        for (String userExp : s.trim().split(" ")) {
            userExpression.add(userExp);
        }
        if (s.contains("+") || s.contains("плюс")) {
            System.out.println("Результат сложения: " + add());
        } else if (s.contains("-") || s.contains("минус")) {
            System.out.println("Результат вычитания: " + substract());
        } else if (s.contains("*") || s.contains("умножить")) {
            System.out.println("Результат умножения: " + multiply());
        } else if (s.contains("/") || s.contains("разделить")) {
            System.out.println("Результат деления: " + divide());
        } else {
            throw new SpacesException("Ошибка 006: не введено действие!");
        }
        userExpression.clear();
    }

    public static double divide() throws NullException { //Деление
        double i = Double.parseDouble(userExpression.get(0)) / Double.parseDouble(userExpression.get(2));
        if (Double.isInfinite(i)) {
            throw new NullException();
        } else {
            return i;
        }
    }

    public static double multiply() { //Умножение
        return Double.parseDouble(userExpression.get(0)) * Double.parseDouble(userExpression.get(2));
    }

    public static double add() { //Сложение
        return Double.parseDouble(userExpression.get(0)) + Double.parseDouble(userExpression.get(2));
    }

    public static double substract() { //Вычитание
        return Double.parseDouble(userExpression.get(0)) - Double.parseDouble(userExpression.get(2));
    }
}
