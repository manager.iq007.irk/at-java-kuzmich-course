import java.util.Scanner;


public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws SpacesException, NullException, NumberException, MaxValueException, ValidException {
        calculate();
    }

    public static void calculate() throws SpacesException, NullException, NumberException, MaxValueException, ValidException {
        System.out.println("Введите выражение в формате \"2 + 3\". " +
                "Допускается писать действия словами и использовать дробные значения");
        while (true) {
            String s = scanner.nextLine();
            UserExpression.checkExpression(s);
            UserExpression.doOperation(s);
            if (s.contains("выход")) {
                break;
            }
        }
    }
}