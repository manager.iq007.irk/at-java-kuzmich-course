public class MaxValueException extends Exception{
    public MaxValueException(String message) {//Превышение максимального значения double
        super(message);
    }
}
