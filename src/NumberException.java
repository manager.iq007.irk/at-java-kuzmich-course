public class NumberException extends Exception {
    public NumberException(String message) { //Не введены числа для расчета, а введены буквы, символы
        super(message);
    }
}
